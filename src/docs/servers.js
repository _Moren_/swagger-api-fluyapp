const { FLLUYAPP_API } = require('../../environment');
module.exports = {
    servers: [
        {
            url: FLLUYAPP_API,
            description: "Api server-test api-manage-fluyapp"
        },
        {
            url:'localhost:8001/api_fluyapp',
            description: "Local server to api-manage-fluyapp"
        }
    ]
}