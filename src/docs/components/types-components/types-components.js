module.exports = {
    gettingsContactsTypes: {
        type: 'object',
        properties: {
            contactType: {
                type: 'string',
                example: [
                    'phone',
                    'whatsapp',
                    'email',
                ]
            }
        }
    },
}