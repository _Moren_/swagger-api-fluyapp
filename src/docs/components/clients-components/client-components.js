module.exports = {
    createClient: {
        type: 'object',
        properties: {
            companyCode: {
                type: 'string',
            },
            category: {
                type: 'string',
            },
            webserviceURL: {
                type: 'string',
            },
            clientID: {
                type: 'string',
            },
            country: {
                type: 'string',
            },
            Uber: {
                type: 'boolean',
            },
            virtual: {
                type: 'boolean',
            },
            subcategories: {
                type: 'string',
            },
            validaterUser: {
                type: 'string',
            },
            debUrl: {
                type: 'string',
            },
            apiType: {
                type: 'string',
            },
            name: {
                type: 'string',
            },
            color: {
                type: 'string',
            },
            serviceType: {
                type: 'string',
            },
            disabled: {
                type: 'boolean',
            },
            contacts: {
                type: 'object',
            },
            order: {
                type: 'number',
            },
        },
    },
    udpdatingClient: {
        type: 'object',
        properties: {
            companyCode: {
                type: 'string',
            },
            category: {
                type: 'string',
            },
            webserviceURL: {
                type: 'string',
            },
            clientID: {
                type: 'string',
            },
            country: {
                type: 'string',
            },
            Uber: {
                type: 'boolean',
            },
            virtual: {
                type: 'boolean',
            },
            subcategories: {
                type: 'string',
            },
            validaterUser: {
                type: 'string',
            },
            debUrl: {
                type: 'string',
            },
            apiType: {
                type: 'string',
            },
            name: {
                type: 'string',
            },
            color: {
                type: 'string',
            },
            serviceType: {
                type: 'string',
            },
            disabled: {
                type: 'boolean',
            },
            contacts: {
                type: 'object',
            },
            order: {
                type: 'number',
            },
        }
    },
    gettingClient: {
        type: 'object',
        properties: {
            companyCode: {
                type: 'string',
            },
            category: {
                type: 'string',
            },
            webserviceURL: {
                type: 'string',
            },
            clientID: {
                type: 'string',
            },
            bgBannerURL: {
                type: 'string',
            },
            logoSmallURL: {
                type: 'string',
            },
            logoHeader: {
                type: 'string',
            },
            logoAltURL: {
                type: 'string',
            },
            country: {
                type: 'string',
            },
            BannerBig: {
                type: 'string',
            },
            Uber: {
                type: 'boolean',
            },
            virtual: {
                type: 'boolean',
            },
            logoBanner: {
                type: 'string',
            },
            subcategories: {
                type: 'string',
            },
            logoURL: {
                type: 'string',
            },
            validaterUser: {
                type: 'string',
            },
            debUrl: {
                type: 'string',
            },
            apiType: {
                type: 'string',
            },
            name: {
                type: 'string',
            },
            color: {
                type: 'string',
            },
            serviceType: {
                type: 'string',
            },
            disabled: {
                type: 'boolean',
            },
            contacts: {
                type: 'object',
            },
            order: {
                type: 'number',
            },
        },
    },
    updateDisableOrEnable: {
        type: 'object',
        properties: {
            disabled: {
                type: 'boolean',
            },
        }
    },
    updateContact: {
        type: 'object',
        properties: {
            contacts: {
                type: 'object',
            },
        }
    },
    deleteContacts: {
        type: 'object',
        properties: {
            contacts: {
                type: 'object',
            },
        }
    },
    updateBanners: {
        type: 'object',
        properties: {
            base64: {
                type: 'string',
            },
        }
    },
    reponseUpdateBanners: {
        type: 'object',
        properties: {
            data: {
                type: 'string',
            },
        }
    }
}