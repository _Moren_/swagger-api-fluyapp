module.exports = {
    createSites: {
        type: 'object',
        properties: {
            name: {
                type: 'string',
            },
            client: {
                type: 'string',
            },
            ADName: {
                type: 'string',
            },
            departmentID: {
                type: 'number',
            },
            disabled:{
                type: 'boolean',
            },
            id: {
                type: 'number',
            },
            customURL: {
                type: 'string',
            },
            address: {
                type: 'string',
            },
            location: {
                type: 'string',
            },
            timezone: {
                type: 'string',
            },
            clientID: {
                type: 'number',
            },
            webserviceURL: {
                type: 'string',
            }
        }
    },
    findByClientById: {
        type: 'object',
        properties: {
            client: {
                type: 'string'
            }
        }
    },
    findSiteById: {
        type: 'object',
        properties: {
            id: {
                type: 'string'
            }
        }
    },
    updateBodySites: {
        type: 'object',
        properties: {
            name: {
                type: 'string',
            },
            client: {
                type: 'string',
            },
            ADName: {
                type: 'string',
            },
            departmentID: {
                type: 'number',
            },
            id: {
                type: 'number',
            },
            bonusTime: {
                type: 'number',
            },
            customURL: {
                type: 'string',
            },
            address: {
                type: 'string',
            },
            location: {
                type: 'string',
            },
            timezone: {
                type: 'string',
            },
            clientID: {
                type: 'number',
            },
            webserviceURL: {
                type: 'string',
            }
        }
    },
    updateBannerSites: {
        type: 'object',
        properties: {
            base64: {
                type: 'string'
            }
        }
    },
    updateStateSite: {
        type: 'object',
        properties: {
            disabled: {
                type: 'boolean'
            }
        }
    }
}