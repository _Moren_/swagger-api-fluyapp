module.exports = {
    createUser: {
        type: 'object',
        properties: {
            name: {
                type: 'string'
            },
            perfil: {
                type: 'string'
            },
            country: {
                type: 'string'
            },
            lastname: {
                type: 'string'
            },
            email: {
                type: 'string'
            },
            client: {
                type: 'object'
            },
            password: {
                type: 'string'
            }
        }
    },
    requestPassword: {
        type: 'object',
        properties: {
            password: {
                type: 'string'
            }
        }
    },
    updateInfoUser: {
        type: 'object',
        properties: {
            name: {
                type: 'string'
            },
            perfil: {
                type: 'string'
            },
            country: {
                type: 'string'
            },
            lastname: {
                type: 'string'
            },
            email: {
                type: 'string'
            },
            client: {
                type: 'object'
            },
        }
    },
    loginUser: {
        type: 'object',
        properties: {
            email: {
                type: 'string'
            },
            password: {
                type: 'string'
            }
        }
    },
    responsesLogin: {
        type: 'object',
        properties: {
            name: {
                type: 'string'
            },
            perfil: {
                type: 'string'
            },
            country: {
                type: 'string'
            },
            lastname: {
                type: 'string'
            },
            email: {
                type: 'string'
            },
            client: {
                type: 'object'
            },
        }
    }
}