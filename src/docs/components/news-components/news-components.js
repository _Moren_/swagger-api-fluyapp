module.exports = {
    createNews: {
        type: 'object',
        properties: {
            banner: {
                type: 'string'
            },
            country: {
                type: 'string'
            },
            link: {
                type: 'string'
            },
            goTo: {
                type: 'string'
            },
            descriptionNews: {
                type: 'string'
            },
        }
    },
    bannerBodyNews: {
        type: 'object',
        properties: {
            banner: {
                type: 'string'
            }
        }
    },
    responseBannerUpdate: {
        type: 'object',
        properties: {
            data: {
                type: 'string'
            }
        }
    },
    updateInfoNews: {
        type: 'object',
        properties: {
            country: {
                type: 'string'
            },
            link: {
                type: 'string'
            },
            goTo: {
                type: 'string'
            },
            descriptionNews: {
                type: 'string'
            },
        }
    }
}