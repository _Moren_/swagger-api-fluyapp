const categories = require('./categories-componenets/cat-components');
const clients = require('./clients-components/client-components');
const sites = require('./sites-components/sites-components');
const news = require('./news-components/news-components');
const types = require('./types-components/types-components');
const users = require('./users-components/users-components');
const dictionaries = require('./dictionary-components/dictionary-components');
const queues = require('./queues-components/queues-components');
const requeriments = require('./requeriments-components/requeriments-components');

module.exports = {
    components: {
        schemas: {
            ...categories,
            ...clients,
            ...sites,
            ...dictionaries,
            ...news,
            ...queues,
            ...users,
            ...types,
            ...requeriments
        }
    }
}