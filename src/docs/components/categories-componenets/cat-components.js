module.exports = {
    createCat: {
        type: 'object',
        properties: {
            name: {
                type: 'string',
            },
            id: {
                type: 'string',
            },
            nameEnglish: {
                type: 'string',
            },
            prioridad: {
                type: 'number',
            },
            disabled: {
                type: 'boolean',
            }
        }
    },
    updateCat: {
        type: 'object',
        properties: {
            name: {
                type: 'string',
            },
            id: {
                type: 'string',
            },
            nameEnglish: {
                type: 'string',
            },
            prioridad: {
                type: 'number',
            },
            disabled: {
                type: 'boolean',
            }
        }
    },
    gettingCat: {
        type: 'object',
        properties: {
            iconURL: {
                type: 'string',
            },
            name: {
                type: 'string',
            },
            id: {
                type: 'string',
            },
            nameEnglish: {
                type: 'string',
            },
            prioridad: {
                type: 'number',
            },
            disabled: {
                type: 'boolean',
            }
        }
    },
    updateBannerCat: {
        type: 'object',
        properties: {
            iconURL: {
                type: 'string',
            },
        }
    }
}