module.exports = {
    createRequeriments: {
        type: 'object',
        properties: {
            clientId: {
                type: 'string'
            },
            name: {
                type: 'string'
            },
            checked: {
                type: 'boolean'
            },
            type: {
                type: 'object'
            },
            form: {
                type: 'object'
            },
            term: {
                type: 'object'
            }
        }
    },
    requestRequirementBody: {
        type: 'object',
        properties: {
            _id: {
                type: 'string'
            },
            clientId: {
                type: 'string'
            },
        }
    },
    updateRequeriment: {
        type: 'object',
        properties: {
            name: {
                type: 'string'
            },
            checked: {
                type: 'boolean'
            },
            type: {
                type: 'object'
            },
            form: {
                type: 'object'
            },
            term: {
                type: 'object'
            }
        }
    },
    requestRequirementBodyDelete: {
        type: 'object',
        properties: {
            clientId: {
                type: 'string'
            }
        }
    }
}