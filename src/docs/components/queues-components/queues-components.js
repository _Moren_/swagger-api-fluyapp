module.exports = {
    createQueue: {
        type: 'object',
        properties: {
            id: {
                type: 'number'
            },
            name: {
                type: 'string'
            },
            clientID: {
                type: 'number'
            },
            webserviceURL: {
                type: 'string'
            },
            serviceType: {
                type: 'string'
            },
            subcategories: {
                type: 'object'
            },
            client: {
                type: 'string'
            },
            typeId: {
                type: 'string'
            },
            alias: {
                type: 'string'
            },
            disabled: {
                type: 'boolean'
            },
            virtual: {
                type: 'boolean'
            },
        }
    },
    gettingsQueues: {
        type: 'object',
        properties: {
            id: {
                type: 'number'
            },
            name: {
                type: 'string'
            },
            clientID: {
                type: 'number'
            },
            webserviceURL: {
                type: 'string'
            },
            status: {
                type: 'string'
            },
            serviceType: {
                type: 'string'
            },
            type: {
                type: 'string'
            },
            requirements: {
                type: 'object'
            },
            sitesAssociates: {
                type: 'object'
            },
            subcategories: {
                type: 'object'
            },
            client: {
                type: 'string'
            },
            typeId: {
                type: 'string'
            },
            alias: {
                type: 'string'
            },
            disabled: {
                type: 'boolean'
            },
            virtual: {
                type: 'boolean'
            },
        }
    },
    addSubCategory: {
        type: 'object',
        properties: {
            subcategories: {
                type: 'object'
            }
        }
    },
    updateSiteAssociated: {
        type: 'object',
        properties: {
            sitesAssociates: {
                type: 'object'
            }
        }
    },
    updateQueueRequeriments: {
        type: 'object',
        properties: {
            requirements: {
                type: 'object',
            }
        }
    },
    updateStateQueue: {
        type: 'object',
        properties: {
            disabled: {
                type: 'boolean'
            }
        }
    }
}