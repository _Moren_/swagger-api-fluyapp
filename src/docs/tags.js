
module.exports = {
    tags: [
        {
            name: 'categories'
        },
        {
            name: 'clients'
        },
        {
            name: 'sites'
        },
        {
            name:'dictionaries'
        },
        {
            name:'news'
        },
        {
            name:'queues'
        },
        {
            name:'users'
        },
        {
            name:'types'
        },
        {
            name:'requeriments'
        }
    ]
}