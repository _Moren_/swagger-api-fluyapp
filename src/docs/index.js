const basicInfo = require('./basicInfo');
const servers = require('./servers');
const components = require('./components/index');
const tags = require('./tags');
const paths = require('./paths/index')

module.exports = {
    ...basicInfo,
    ...servers,
    ...components,
    ...tags,
    ...paths

}