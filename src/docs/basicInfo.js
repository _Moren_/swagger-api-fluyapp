module.exports = {
    openapi: "3.0.1",
    info: {
        version: "1.0.0",
        title: "Fluyapp",
        description: "Fluyapp API",
        contact: {
            email: "francisco@getxplor.com"
        }
    }
}