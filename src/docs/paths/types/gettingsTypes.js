module.exports = {
    get: {
        tags: ['types'],
        description: "",
        operationId: "gettingsTypes",
        responses: {
            'API_TC_200': {
                description: 'Tipos de contactos.',
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/gettingsContactsTypes'
                        }
                    }
                }
            },
        }
    }
}