const gettingsTypes = require('./gettingsTypes');

module.exports = {
    '/contact-type-list/': {
        ...gettingsTypes
    }
}