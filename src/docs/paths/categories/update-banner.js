module.exports = {
    post: {
        tags: ['categories'],
        description: "Update banner category",
        operationId: "updateBannerCat",
        parameters: [
            {
                in: 'query',
                name: 'id'
            }
        ],
        requestBody: {
            content: {
                'application/json': {
                    schema: {
                        $ref: '#/components/schemas/updateBannerCat'
                    }
                }
            }
        },
        responses: {
            'API_CTG_200': {
                description: 'Imagen actualizada con éxito',
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/updateBannerCat'
                        }
                    }
                }
            },
            'API_CTG_403': {
                description: 'Fallo al subir la imagen'
            },
            'API_CTG_403': {
                description: 'Fallo al guardar la categoria.'
            }
        }
    }
}
