module.exports = {
    post: {
        tags: ['categories'],
        description: "Create category",
        operationId: "createCat",
        requestBody: {
            content: {
                'application/json': {
                    schema: {
                        $ref: '#/components/schemas/createCat'
                    }
                }
            }
        },
        responses: {
            'API_CTG_401': {
                description: 'Fallo al registrar una nueva categoría.'
            },
            'API_CTG_200': {
                description: 'Registro de Categoría con éxito.',
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/createCat'
                        }
                    }
                }
            },
            'E11000': {
                description: 'Ya existe una Categoría con ese nombre.'
            }
        }
    }
}