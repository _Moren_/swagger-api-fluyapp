const getCatogory = require('./get-category');
const updateCategory = require('./update-category');
const createCategory = require('./create-category');
const updateBannterCat = require('./update-banner');

module.exports = {
    '/category-create/': {
        ...createCategory
    },
    '/category-lists': {
        ...getCatogory
    },
    '/category-update/:id': {
        ...updateCategory
    },
    '/image-upload-category': {
        ...updateBannterCat
    }
}