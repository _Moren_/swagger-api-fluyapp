module.exports = {
    get: {
        tags: ['categories'],
        description: "Update category",
        operationId: "gettingCat",
        paraments: [],
        responses: {
            'API_CTG_404': {
                description: 'No hay registros sobre Categorías.'
            },
            'API_CTG_200': {
                description: 'Listado de las Categorias.',
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/gettingCat'
                        }
                    }
                }
            }
        }
    }
}