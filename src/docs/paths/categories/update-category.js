module.exports = {
    put: {
        tags: ['categories'],
        description: "Update category",
        operationId: "updateCat",
        parameters: [
            {
                in: 'params',
                name: 'id'
            }
        ],
        requestBody: {
            content: {
                'application/json': {
                    schema: {
                        $ref: '#/components/schemas/updateCat'
                    }
                }
            }
        },
        responses: {
            'API_CTG_401': {
                description: 'El identificador de la Categoría a actualizar no existe.'
            },
            'API_CTG_200': {
                description: 'Categoria actualizada con éxito.',
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/createCat'
                        }
                    }
                }
            },
            'E11000': {
                description: 'Ya existe una Categoría con ese nombre.'
            }
        }
    }
}