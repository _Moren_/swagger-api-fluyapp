module.exports = {
    post: {
        tags: ['clients'],
        description: "Create client",
        operationId: "createClient",
        requestBody: {
            content: {
                'application/json': {
                    schema: {
                        $ref: '#/components/schemas/createClient'
                    }
                }
            }
        },
        responses: {
            'API_C_401': {
                description: 'Error al registrar nuevo cliente.'
            },
            'API_C_200': {
                description: 'Registro exitoso.',
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/createClient'
                        }
                    }
                }
            },
            'E11000': {
                description: 'Ya existe un client con ese nombre.'
            }
        }
    }
}