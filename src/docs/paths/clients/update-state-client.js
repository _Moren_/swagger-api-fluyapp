module.exports = {
    put: {
        tags: ['clients'],
        description: "Habilita o deshabilitad estado del cliente",
        operationId: "udpdatingClient",
        requestBody: {
            content: {
                'application/json': {
                    schema: {
                        $ref: '#/components/schemas/updateDisableOrEnable'
                    }
                }
            }
        },
        responses: {
            'API_C_401': {
                description: 'No se pudo cambiar el estado del cliente. Intente de nuevo.'
            },
            'API_C_200': {
                description: 'El estado del cliente fue cambiado con éxito.',
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/gettingClient'
                        }
                    }
                }
            },
            'E11000': {
                description: 'Ya existe un client con ese nombre.'
            }
        }
    }
}