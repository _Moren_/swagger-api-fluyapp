module.exports = {
    put: {
        tags: ['clients'],
        description: "Actualiza cierta información acerca del cliente.",
        operationId: "update Client",
        parameters: [
            {
                in: 'params',
                name: 'id'
            }
        ],
        requestBody: {
            content: {
                'application/json': {
                    schema: {
                        $ref: '#/components/schemas/udpdatingClient'
                    }
                }
            }
        },
        responses: {
            'API_C_401': {
                description: 'Error al actualizar los datos del cliente'
            },
            'API_C_200': {
                description: 'Cliente actualizado con éxito.',
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/gettingClient'
                        }
                    }
                }
            },
        }
    }
}