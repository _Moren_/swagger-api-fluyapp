module.exports = {
    get: {
        tags: ['clients'],
        description: "Busca client por medio de su nombre.",
        operationId: "searchClientByName",
        parameters: [
            {
                in: 'body',
                name: 'search',
                type: 'string'
            }
        ],
        responses: {
            'API_C_200': {
                description: 'Información del cliente.',
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/gettingClient'
                        }
                    }
                }
            },
            'API_C_401': {
                description: 'No existe este cliente'
            }
        }
    }
}