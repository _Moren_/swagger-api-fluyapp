module.exports = {
    get: {
        tags: ['clients'],
        description: "Se obtiene el listado de clientes en base al tipo de perfil.",
        operationId: "gettingClient",
        responses: {
            'API_C_200': {
                description: 'Listado de clientes con éxito.',
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/gettingClient'
                        }
                    }
                }
            },
            'API_C_401': {
                description: 'No hay datos relacionados a este cliente.'
            }
        }
    }
}