module.exports = {
    put: {
        tags: ['clients'],
        description: "Agregar valores de contacto al cliente.",
        operationId: "update Contact",
        parameters: [
            {
                in: 'params',
                name: '_id'
            }
        ],
        requestBody: {
            content: {
                'application/json': {
                    schema: {
                        $ref: '#/components/schemas/updateContact'
                    }
                }
            }
        },

        responses: {
            'API_C_401': {
                description: 'No fue posible agregar el contacto. Intente de nuevo.'
            },
            'API_C_200': {
                description: 'Contacto agregado con éxito.',
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/gettingClient'
                        }
                    }
                }
            },
        }
    }
}