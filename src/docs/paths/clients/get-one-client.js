module.exports = {
    get: {
        tags: ['clients'],
        description: "Se obtiene los valores de cliente por medio de su id.",
        operationId: "gettingOne",
        parameters: [
            {
                in: 'params',
                name: 'id'
            }
        ],
        responses: {
            'API_C_200': {
                description: 'Información del cliente.',
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/gettingClient'
                        }
                    }
                }
            },
            'API_C_401': {
                description: 'No hay datos relacionados a este cliente.'
            }
        }
    }
}