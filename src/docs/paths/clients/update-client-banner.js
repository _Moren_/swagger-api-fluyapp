module.exports = {
    post: {
        tags: ['clients'],
        description: "Actualiza uno de las imagenes del cliente.",
        operationId: "update Banners",
        parameters: [
            {
                in:'query',
                name:'_id'
            }
        ],
        requestBody: {
            content: {
                'application/json': {
                    schema: {
                        $ref: '#/components/schemas/updateBanners'
                    }
                }
            }
        },
        responses: {
            'API_CI_403': {
                description: 'Imagen actualizada con éxito'
            },
            'API_CI_200': {
                description: 'Registro exitoso.',
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/reponseUpdateBanners'
                        }
                    }
                }
            }
        }
    }
}