module.exports = {
    put: {
        tags: ['clients'],
        description: "Elimina contacto de un cliente en base a su id.",
        operationId: "deleteContact",
        parameters: [
            {
                in: 'params',
                name: '_id',
                type: 'string'
            }
        ],
        requestBody: {
            content: {
                'application/json': {
                    schema: {
                        $ref: '#/components/schemas/deleteContacts'
                    }
                }
            }
        },
        responses: {
            'API_C_401': {
                description: 'No se pudo cambiar el estado del cliente. Intente de nuevo.'
            },
            'API_C_200': {
                description: 'Contacto eliminado con éxito.',
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/gettingClient'
                        }
                    }
                }
            }
        }
    }
}