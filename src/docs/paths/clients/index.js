const createClient = require('./create-client');
const updateClient = require('./update-client');
const updateContact = require('./update-client-contact');
const updateClientState = require('./update-state-client');
const deleteClient = require('./delete-client');
const getsClientProfile = require('./get-client-profile');
const getDetailClient = require('./get-one-client');
const getClientByName = require('./get-client-by-name');
const updateClientsBanners = require('./update-client-banner');

module.exports = {
    '/clients-lists': {
        ...getsClientProfile
    },
    '/clients-view/:id': {
        ...getDetailClient
    },
    '/clients-create': {
        ...createClient
    },
    '/clients-update/:id': {
        ...updateClient
    },
    '/clients-search/': {
        ...getClientByName
    },
    '/clients-delete/:_id': {
        ...deleteClient
    },
    '/clients-add-contact/:_id': {
        ...updateContact
    },
    '/clients-del-contact/:_id': {
        ...updateClientState
    },
    '/image-upload-customer': {
        ...updateClientsBanners
    }
}

