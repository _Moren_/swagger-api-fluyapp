module.exports = {
    put: {
        tags: ['requeriments'],
        description: "",
        operationId: "deleteRequeriment",
        parameters: [
            {
                in: 'params',
                name: '_id'
            }
        ],
        requestBody: {
            content: {
                'application/json': {
                    schema: {
                        $ref: '#/components/schemas/requestRequirementBodyDelete'
                    }
                }
            }
        },
        responses: {
            'API_RQT_401': {
                description: 'No se eliminó el requerimento.'
            },
            'API_RQT_200': {
                description: 'Requerimineto eliminado con éxito.',
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/createRequeriments'
                        }
                    }
                }
            },
        }
    }
}