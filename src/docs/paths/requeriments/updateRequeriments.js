module.exports = {
    put: {
        tags: ['requeriments'],
        description: "",
        operationId: "updateRequeriment",
        requestBody: {
            content: {
                'application/json': {
                    schema: {
                        $ref: '#/components/schemas/updateRequeriment'
                    }
                }
            }
        },
        responses: {
            'API_RQT_401': {
                description: 'No pudo ser actualizar el requerimiento. Intente de nuevo'
            },
            'API_RQT_200': {
                description: 'Requerimineto actualziado con éxito.',
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/createRequeriments'
                        }
                    }
                }
            },
        }
    }
}