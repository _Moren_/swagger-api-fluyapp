module.exports = {
    post: {
        tags: ['requeriments'],
        description: "",
        operationId: "createRequeriments",
        requestBody: {
            content: {
                'application/json': {
                    schema: {
                        $ref: '#/components/schemas/createRequeriments'
                    }
                }
            }
        },
        responses: {
            'API_RQT_401': {
                description: 'No pudo ser registrado el requerimiento. Intente de nuevo'
            },
            'API_RQT_200': {
                description: 'Requerimineto registrado con éxito.',
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/createRequeriments'
                        }
                    }
                }
            },
        }
    }
}