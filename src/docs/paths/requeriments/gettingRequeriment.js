module.exports = {
    post: {
        tags: ['requeriments'],
        description: "",
        operationId: "gettingRequeriment",
        requestBody: {
            content: {
                'application/json': {
                    schema: {
                        $ref: '#/components/schemas/requestRequirementBody'
                    }
                }
            }
        },
        responses: {
            'API_RQT_401': {
                description: 'No existe el requerimiento a buscar.'
            },
            'API_RQT_200': {
                description: 'Detalles del requerimineto.',
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/createRequeriments'
                        }
                    }
                }
            },
        }
    }
}