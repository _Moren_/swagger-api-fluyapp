module.exports = {
    get: {
        tags: ['requeriments'],
        description: "",
        operationId: "gettingsRequeriments",
        parameters: [
            {
                in: 'params',
                name: '_id'
            }
        ],
        responses: {
            'API_RQT_404': {
                description: 'No hay requerimientos registrados.'
            },
            'API_RQT_200': {
                description: 'Listado de requerimientos.',
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/createRequeriments'
                        }
                    }
                }
            },
        }
    }
}