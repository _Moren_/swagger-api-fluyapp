const createRequeriments = require('./createRequeriments');
const gettingsRequeriments = require('./gettingsRequeriments');
const gettingRequeriment = require('./gettingRequeriment');
const updateRequeriment = require('./updateRequeriments');
const deleteRequeriment = require('./deleteRequeriment');

module.exports = {
    '/requirement-create/': {
        ...createRequeriments
    },
    '/requirement-update/:_id': {
        ...updateRequeriment
    },
    '/requirement-lists/:_id': {
        ...gettingsRequeriments
    },
    '/requirement-view': {
        ...gettingRequeriment
    },
    '/requirement-delete/:_id': {
        ...deleteRequeriment
    }
}