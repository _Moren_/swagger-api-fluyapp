const clients = require('./clients/index');
const categories = require('./categories/index');
const sites = require('./sites/index');
const dictionaries = require('./dictionaries/index');
const news = require('./news/index');
const users = require('./users/index');
const types = require('./types/index');
const queues = require('./queues/index');
const requeriments = require('./requeriments/index');

module.exports = {
    paths: {
        ...categories,
        ...clients,
        ...sites,
        ...dictionaries,
        ...news,
        ...queues,
        ...users,
        ...types,
        ...requeriments
    }
}