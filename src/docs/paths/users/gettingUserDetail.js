module.exports = {
    get: {
        tags: ['users'],
        description: "",
        operationId: "gettingUser",
        parameters: [
            {
                in: 'params',
                name: '_id'
            }
        ],
        responses: {
            'API_U_403': {
                description: 'El usuario no existe.'
            },
            'API_U_200': {
                description: 'Información del usuario.',
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/createUser'
                        }
                    }
                }
            },
        }
    }
}