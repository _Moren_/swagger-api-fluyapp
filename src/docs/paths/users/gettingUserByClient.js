module.exports = {
    get: {
        tags: ['users'],
        description: "",
        operationId: "gettingUsersUserClient",
        parameters: [
            {
                in: 'params',
                name: 'client'
            }
        ],
        responses: {
            'API_U_403': {
                description: 'No hay usuarios a mostrar.'
            },
            'API_U_200': {
                description: 'Listado de usuario con éxito.',
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/createUser'
                        }
                    }
                }
            },
        }
    }
}