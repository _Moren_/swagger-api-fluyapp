module.exports = {
    put: {
        tags: ['users'],
        description: "",
        operationId: "updateInfoUser",
        parameters: [
            {
                in: 'params',
                name: 'id'
            }
        ],
        requestBody: {
            content: {
                'application/json': {
                    schema: {
                        $ref: '#/components/schemas/updateInfoUser'
                    }
                }
            }
        },
        responses: {
            'API_U_401': {
                description: 'El dato a buscar no existe'
            },
            'API_U_402': {
                description: 'Fallo al actualizar el usuario.'
            },
            'API_U_200': {
                description: 'Usuario actualizado con éxito.',
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/createUser'
                        }
                    }
                }
            },
        }
    }
}