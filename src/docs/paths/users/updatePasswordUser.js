module.exports = {
    put: {
        tags: ['users'],
        description: "",
        operationId: "updatePasswordUser",
        parameters: [
            {
                in: 'params',
                name: 'id'
            }
        ],
        requestBody: {
            content: {
                'application/json': {
                    schema: {
                        $ref: '#/components/schemas/requestPassword'
                    }
                }
            }
        },
        responses: {
            'API_U_401': {
                description: 'Error al modificar su contraseña.'
            },
            'API_U_402': {
                description: 'Este correo ya existe.'
            },
            'API_U_200': {
                description: 'Cambio de contraseña con éxito.',
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/createUser'
                        }
                    }
                }
            },
        }
    }
}