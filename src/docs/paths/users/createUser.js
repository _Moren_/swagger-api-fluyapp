module.exports = {
    post: {
        tags: ['users'],
        description: "",
        operationId: "createUser",
        requestBody: {
            content: {
                'application/json': {
                    schema: {
                        $ref: '#/components/schemas/createUser'
                    }
                }
            }
        },
        responses: {
            'API_U_401': {
                description: 'Error al registrar nuevo usuario.'
            },
            'API_U_200': {
                description: 'Registro exitoso.',
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/createUser'
                        }
                    }
                }
            },
        }
    }
}