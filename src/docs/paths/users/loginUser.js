module.exports = {
    post: {
        tags: ['users'],
        description: "",
        operationId: "loginUser",
        requestBody: {
            content: {
                'application/json': {
                    schema: {
                        $ref: '#/components/schemas/loginUser'
                    }
                }
            }
        },
        responses: {
            'API_U_404': {
                description: 'No existe este correo en los registros.'
            },
            'API_U_402': {
                description: 'Contraseña incorrecta. Vuelva a intentar.'
            },
            'API_U_200': {
                description: 'Inicio de sesion exitosa.',
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/responsesLogin'
                        }
                    }
                }
            },
        }
    }
}