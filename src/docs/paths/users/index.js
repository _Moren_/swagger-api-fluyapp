const createUser = require('./createUser');
const updateInfoUser = require('./updateInfoUser');
const gettingUser = require('./gettingUserDetail');
const loginUser = require('./loginUser');
const updateOnlyPassword = require('./updatePasswordUser');
const gettingUsersUserClient = require('./gettingUserByClient');

module.exports = {
    '/user-create/': {
        ...createUser
    },
    '/user-update-pass/:id': {
        ...updateOnlyPassword
    },
    '/user-update/:id': {
        ...updateInfoUser
    },
    '/user-lists/:client': {
        ...gettingUsersUserClient
    },
    '/user-view/:_id': {
        ...gettingUser
    },
    '/user-login/': {
        ...loginUser
    }
}