module.exports = {
    put: {
        tags: ['queues'],
        description: "",
        operationId: "updateQueuesSiteAssociated",
        parameters: [
            {
                in: 'params',
                name: 'idClient'
            }
        ],
        requestBody: {
            content: {
                'application/json': {
                    schema: {
                        $ref: '#/components/schemas/updateSiteAssociated'
                    }
                }
            }
        },
        responses: {
            'API_Q_401': {
                description: 'el cliente no tiene esos servicios asociados.'
            },
            'API_Q_200': {
                description: 'Colas actualizadas con éxito.',
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/createQueue'
                        }
                    }
                }
            },
        }
    }
}