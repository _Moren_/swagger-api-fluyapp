module.exports = {
    put: {
        tags: ['queues'],
        description: "",
        operationId: "updateQueue",
        parameters: [
            {
                in: 'params',
                name: 'id'
            }
        ],
        requestBody: {
            content: {
                'application/json': {
                    schema: {
                        $ref: '#/components/schemas/createQueue'
                    }
                }
            }
        },
        responses: {
            'API_Q_401': {
                description: 'Fallo al intentar actualizar la cola.'
            },
            'API_Q_200': {
                description: 'Cola actualizada con éxito.',
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/createQueue'
                        }
                    }
                }
            },
        }
    }
}