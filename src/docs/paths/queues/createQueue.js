module.exports = {
    post: {
        tags: ['queues'],
        description: "",
        operationId: "createQueue",
        requestBody: {
            content: {
                'application/json': {
                    schema: {
                        $ref: '#/components/schemas/createQueue'
                    }
                }
            }
        },
        responses: {
            'API_Q_401': {
                description: 'No hay datos relacionados con las colas.'
            },
            'API_Q_200': {
                description: 'Información de las colas.',
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/gettingsQueues'
                        }
                    }
                }
            },
        }
    }
}