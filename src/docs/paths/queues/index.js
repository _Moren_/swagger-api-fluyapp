const createNewQueue = require('./createQueue');
const updateQueue = require('./updateQueue');
const deleteQueue = require('./deleteQueue');
const gettingQueueDetail = require('./getDetailQueue');
const listQueuesSites = require('./listingQueueByClient');
const addSubCategoryQueue = require('./addSubcategoryQueue');
const updateSiteAssociated = require('./updatesiteAssociated');
const updateQueueRequeriments = require('./updateQueueRequeriments');
const updateQueuesSiteAssociated = require('./updateQueuesSitesAssociates');

module.exports = {
    '/queue-create/': {
        ...createNewQueue
    },
    '/queue-lists/:_id': {
        ...listQueuesSites
    },
    '/queue-update/:id': {
        ...updateQueue
    },
    '/queue-add-subcat/:_id': {
        ...addSubCategoryQueue
    },
    '/update-queue-siteassociates/:_id': {
        ...updateQueuesSiteAssociated
    },
    '/queue-add-requirement/:_id': {
        ...updateQueueRequeriments
    },
    '/queue-add-site/:_id': {
        ...updateSiteAssociated
    },
    '/queue-delete/:_id': {
        ...deleteQueue
    },
    '/queue-view/:_id': {
        ...gettingQueueDetail
    }
}