module.exports = {
    get: {
        tags: ['queues'],
        description: "",
        operationId: "gettingQueueDetail",
        parameters: [
            {
                in: 'params',
                name: '_id'
            }
        ],
        responses: {
            'API_Q_401': {
                description: 'No hay valor asociado a ese identificador.'
            },
            'API_Q_200': {
                description: 'Detalles de la cola.',
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/createQueue'
                        }
                    }
                }
            },
        }
    }
}