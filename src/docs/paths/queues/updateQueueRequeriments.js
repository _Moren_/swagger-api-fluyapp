module.exports = {
    put: {
        tags: ['queues'],
        description: "",
        operationId: "updateQueueRequeriments",
        parameters: [
            {
                in: 'params',
                name: '_id'
            }
        ],
        requestBody: {
            content: {
                'application/json': {
                    schema: {
                        $ref: '#/components/schemas/updateQueueRequeriments'
                    }
                }
            }
        },
        responses: {
            'API_Q_401': {
                description: 'No se pudo actualizar los requerimientos.'
            },
            'API_Q_200': {
                description: 'Requerimientos actualizados con éxito.',
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/createQueue'
                        }
                    }
                }
            },
        }
    }
}