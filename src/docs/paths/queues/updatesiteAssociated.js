module.exports = {
    put: {
        tags: ['queues'],
        description: "",
        operationId: "updateSiteAssociated",
        parameters: [
            {
                in: 'params',
                name: '_id'
            }
        ],
        requestBody: {
            content: {
                'application/json': {
                    schema: {
                        $ref: '#/components/schemas/updateSiteAssociated'
                    }
                }
            }
        },
        responses: {
            'API_Q_401': {
                description: 'No se pudo actualizar las sucursales en la cola.'
            },
            'API_Q_200': {
                description: 'Sucursales actualizadas en cola con éxito.',
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/createQueue'
                        }
                    }
                }
            },
        }
    }
}