module.exports = {
    put: {
        tags: ['queues'],
        description: "",
        operationId: "addSubCategoryQueue",
        parameters: [
            {
                in: 'params',
                name: '_id'
            }
        ],
        requestBody: {
            content: {
                'application/json': {
                    schema: {
                        $ref: '#/components/schemas/addSubCategory'
                    }
                }
            }
        },
        responses: {
            'API_Q_401': {
                description: 'Fallo al intentar actualizar la cola.'
            },
            'API_Q_200': {
                description: 'Cola actualizada con éxito.',
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/createQueue'
                        }
                    }
                }
            },
        }
    }
}