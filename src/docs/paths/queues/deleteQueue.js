module.exports = {
    put: {
        tags: ['queues'],
        description: "",
        operationId: "deleteQueue",
        parameters: [
            {
                in: 'params',
                name: '_id'
            }
        ],
        requestBody: {
            content: {
                'application/json': {
                    schema: {
                        $ref: '#/components/schemas/updateStateQueue'
                    }
                }
            }
        },
        responses: {
            'API_Q_401': {
                description: 'No se pudo eliminar la cola. Pruebe de nuevo.'
            },
            'API_Q_200': {
                description: 'Cola eliminada con éxito.',
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/createQueue'
                        }
                    }
                }
            },
        }
    }
}