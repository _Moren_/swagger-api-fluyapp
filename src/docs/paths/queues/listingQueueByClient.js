module.exports = {
    get: {
        tags: ['queues'],
        description: "",
        operationId: "gettingsQueueByClient",
        parameters: [
            {
                in: 'params',
                name: '_id'
            }
        ],
        responses: {
            'API_Q_401': {
                description: 'No hay datos relacionados con las colas.'
            },
            'API_Q_200': {
                description: 'Información de las colas.',
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/gettingsQueues'
                        }
                    }
                }
            },
        }
    }
}