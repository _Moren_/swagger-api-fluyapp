module.exports = {
    put: {
        tags: ['sites'],
        description: "Habilita o deshabilita sucursal para que no aparezca en la app móvil.",
        operationId: "updateStateSite",
        parameters: [
            {
                in: 'params',
                name: '_id'
            }
        ],
        requestBody: {
            description: "Valor para actualizar el estado de una sucursal.",
            content: {
                'applicaction/json': {
                    schema: {
                        $ref: '#/components/schemas/updateStateSite'
                    }
                }
            }
        },
        responses: {
            'API_S_401': {
                description: 'No se pudo eliminar la sucursal. Intente de nuevo.'
            },
            'API_S_200': {
                description: 'Sucursal eliminada con éxito.',
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/createSites'
                        }
                    }
                }
            },
        }
    }
}

