module.exports = {
    get: {
        tags: ['sites'],
        description: "Detalles de la sucursal por medio del id de la misma.",
        operationId: "detailClientSitesById",
        parameters: [
            {
                in:'params',
                name:'client'
            }
        ],          
        responses: {
            'API_S_401': {
                description: 'No hay datos relacionados sobre este sitio.'
            },
            'API_S_200': {
                description: 'Información del sitio.',
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/createSites'
                        }
                    }
                }
            },
            'API_S_401': {
                description: 'No hay datos relacionados todos los sitios.'
            }
        }
    }
}