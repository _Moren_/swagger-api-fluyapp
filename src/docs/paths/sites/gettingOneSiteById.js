module.exports = {
    get: {
        tags: ['sites'],
        description: "Detalles de la sucursal por medio del id de la misma.",
        operationId: "detailSitesById",
        parameters: [
            {
                in:'params',
                name:'id'
            }
        ],
        responses: {
            'API_S_401': {
                description: 'No hay datos relacionados sobre este sitio.'
            },
            'API_S_200': {
                description: 'Información del Local.',
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/createSites'
                        }
                    }
                }
            },
        }
    }
}