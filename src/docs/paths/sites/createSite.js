module.exports = {
    post: {
        tags: ['sites'],
        description: "Creacipon de una sucursal.",
        operationId: "createSite",
        requestBody: {
            content: {
                'application/json': {
                    schema: {
                        $ref: '#/components/schemas/createSites'
                    }
                }
            }
        },
        responses: {
            'API_S_401': {
                description: 'El tiempo debe ser mayor a 5 minutos'
            },
            'API_S_200': {
                description: 'Registro de nuevo Local con éxito.',
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/createSites'
                        }
                    }
                }
            },
            'API_S_401': {
                description: 'Fallo al registrar un nuevo Local.'
            }
        }
    }
}