const createSites = require('./createSite');
const listingSites = require('./gettingsSites');
const gettingSite = require('./gettingSiteById');
const updateSite = require('./updateSites');
const updateBannerSite = require('./updateBannerSite');
const updateStatesite = require('./udateStateSite');

module.exports = {
    '/sites-create/': {
        ...createSites,
    },
    '/sites-view/:id': {
        ...gettingSite
    },
    '/sites-lists/:client': {
        ...listingSites
    },
    '/sites-update/:id': {
        ...updateSite
    },
    '/sites-update-banner/:_id': {
        ...updateBannerSite
    },
    '/sites-delete/:_id': {
        ...updateStatesite
    }
}