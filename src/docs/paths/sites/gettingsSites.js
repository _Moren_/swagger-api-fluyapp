module.exports = {
    get: {
        tags: ['sites'],
        description: "Lista todas las sucursales por medio del id del cliente.",
        operationId: "listSitesByClientId",
        parameters: [
            {
                in:'params',
                name:'id'
            }
        ],
        responses: {
            'API_S_401': {
                description: 'No hay datos relacionados sobre este sitio.'
            },
            'API_S_200': {
                description: 'Información del sitio.',
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/createSites'
                        }
                    }
                }
            },
            'API_S_401': {
                description: 'No hay datos relacionados todos los sitios.'
            }
        }
    }
}