module.exports = {
    put: {
        tags: ['sites'],
        description: "Actualiza el banner (imagen) de la sucursal.",
        operationId: "updateBannerSiteById",
        parameters: [
            {
                in: 'params',
                name: '_id'
            }
        ],
        requestBody: {
            description: "Valor para actualizar la imagen de una sucursal.",
            content: {
                'applicaction/json': {
                    schema: {
                        $ref: '#/components/schemas/updateBannerSites'
                    }
                }
            }
        },
        responses: {
            'API_S_403': {
                description: 'Hubo un error al subir la imagen. Pruebe de nuevo.'
            },
            'API_S_401': {
                description: 'Fallo al actualizar el banner.'
            },
            'API_S_200': {
                description: 'Se ha actualizado el banner de manera exitosa.',
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/createSites'
                        }
                    }
                }
            },
        }
    }
}

