module.exports = {
    get: {
        tags: ['dictionaries'],
        description: "Lista todas las etiquetas sin ningún tipo de filtro.",
        operationId: "gettingDictionaries",
        responses: {
            'API_D_403': {
                description: 'No existe el registros.'
            },
            'API_D_200': {
                description: 'Listado de las etiquetas.',
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/createDictionary'
                        }
                    }
                }
            },
        }
    }
}

