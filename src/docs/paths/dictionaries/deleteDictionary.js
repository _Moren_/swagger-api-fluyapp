module.exports = {
    delete: {
        tags: ['dictionaries'],
        description: "Elimina en valor de una etiqueta.",
        operationId: "deleteDictionary",
        parameters: [
            {
                in: 'params',
                name: '_id',
            },
            {
                in: 'params',
                name: 'client'
            }
        ],
        responses: {
            'API_D_403': {
                description: 'No existe el valor seleccionado.'
            },
            'API_D_200': {
                description: 'Etiqueta eliminada.',
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/createDictionary'
                        }
                    }
                }
            },
        }
    }
}
