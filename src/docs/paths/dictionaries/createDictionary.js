module.exports = {
    post: {
        tags: ['dictionaries'],
        description: "",
        operationId: "createDictionary",
        requestBody: {
            description: "Valor para crear una etiqueta.",
            content: {
                'applicaction/json': {
                    schema: {
                        $ref: '#/components/schemas/createDictionary'
                    }
                }
            }
        },
        responses: {
            'API_D_403': {
                description: 'Registro no realizado. Intente de nuevo.'
            },
            'API_D_403': {
                description: 'Registros duplicados.'
            },
            'API_D_200': {
                description: 'Etiqueta registrada con éxito.',
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/createDictionary'
                        }
                    }
                }
            },
        }
    }
}

