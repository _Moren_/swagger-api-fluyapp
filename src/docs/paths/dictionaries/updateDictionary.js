module.exports = {
    put: {
        tags: ['dictionaries'],
        description: "Actuliza en valor de una etiqueta.",
        operationId: "updateDictionary",
        parameters: [
            {
                in: 'params',
                name: '_id'
            }
        ],
        requestBody: {
            content: {
                'application/json': {
                    schema: {
                        $ref: '#/components/schemas/createDictionary'
                    }
                }
            }
        },
        responses: {
            'API_D_403': {
                description: 'No se pudo actualizar la etiqueta seleccionada.'
            },
            'API_D_200': {
                description: 'Etiqueta actualizada.',
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/createDictionary'
                        }
                    }
                }
            },
        }
    }
}
