const createDictionary = require('./createDictionary');
const gettingDictionartById = require('./gettingDictionartById');
const gettingDictionaries = require('./gettingDictionaries');
const updateDictionary = require('./updateDictionary');
const deleteDictionary = require('./deleteDictionary');

module.exports = {
    '/label-create': {
        ...createDictionary
    },
    '/label-view/:_id': {
        ...gettingDictionartById
    },
    '/label-lists': {
        ...gettingDictionaries
    },
    '/label-delete/:_id/:client/': {
        ...deleteDictionary
    },
    '/label-update/:_id': {
        ...updateDictionary
    }
}