module.exports = {
    get: {
        tags: ['dictionaries'],
        description: "Retorna los detalles de una sola etiqueta según su id.",
        operationId: "gettingDictionaryById",
        parameters: [
            {
                in: 'params',
                name: '_id'
            }
        ],
        responses: {
            'API_D_403': {
                description: 'No existe el registro.'
            },
            'API_D_200': {
                description: 'Detalle de la etiqueta.',
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/createDictionary'
                        }
                    }
                }
            },
        }
    }
}

