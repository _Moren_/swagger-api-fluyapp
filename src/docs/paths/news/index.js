const createNews = require('./create-news');
const updateBannerNews = require('./updateBannerNews');
const updateInfoNews = require('./updateInfoNews');
const gettingsNews = require('./gettingsNews');
const gettingNews = require('./gettingDetailNews');
const deleteNews = require('./deleteNews');

module.exports = {
    '/news-create': {
        ...createNews
    },
    '/news-update/:_id': {
        ...updateInfoNews
    },
    '/news-upload-img/': {
        ...updateBannerNews
    },
    '/news-lists': {
        ...gettingsNews
    },
    '/news-view/:_id': {
        ...gettingNews
    },
    '/news-delete/:_id': {
        ...deleteNews
    }
}