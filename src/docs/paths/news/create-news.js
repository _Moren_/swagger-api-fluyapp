module.exports = {
    post: {
        tags: ['news'],
        description: "",
        operationId: "createNews",
        requestBody: {
            description: "Valor para crear una noticia.",
            content: {
                'applicaction/json': {
                    schema: {
                        $ref: '#/components/schemas/createNews'
                    }
                }
            }
        },
        responses: {
            'API_NW_401': {
                description: 'No se pudo eliminar la sucursal. Intente de nuevo.'
            },
            'API_NW_200': {
                description: 'Noticia general creada con éxito.',
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/createNews'
                        }
                    }
                }
            },
        }
    }
}

