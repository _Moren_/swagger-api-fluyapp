module.exports = {
    delete: {
        tags: ['news'],
        description: "",
        operationId: "deleteNews",
        parameters: [
            {
                in: 'params',
                name: '_id'
            }
        ],
        responses: {
            'API_NW_401': {
                description: 'No se existe la noticia buscada.'
            },
            'API_NW_200': {
                description: 'Noticia eliminada.',  
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/createNews'
                        }
                    }
                }
            },
        }
    }
}
