module.exports = {
    put: {
        tags: ['news'],
        description: "",
        operationId: "updateInfoNews",
        parameters: [
            {
                in: 'params',
                name: '_id  '
            }
        ],
        requestBody: {
            description: "Valores para actualizar información de una noticia.",
            content: {
                'applicaction/json': {
                    schema: {
                        $ref: '#/components/schemas/updateInfoNews'
                    }
                }
            }
        },
        responses: {
            'API_NW_401': {
                description: 'No se pudo actualizar los datos. Pruebe de nuevo.'
            },
            'API_NW_200': {
                description:  'Datos actualizados con éxito.',
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/createNews'
                        }
                    }
                }
            },
        }
    }
}

