module.exports = {
    get: {
        tags: ['news'],
        description: "",
        operationId: "gettingNews",
        parameters: [
            {
                in: 'params',
                name: '_id'
            }
        ],
        responses: {
            'API_NW_401': {
                description: 'No se existe la noticia buscada.'
            },
            'API_NW_200': {
                description: 'Detalles de la noticia.',
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/createNews'
                        }
                    }
                }
            },
        }
    }
}
