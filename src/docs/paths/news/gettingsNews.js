module.exports = {
    get: {
        tags: ['news'],
        description: "",
        operationId: "gettingsNews",
        responses: {
            'API_NW_401': {
                description: 'No se han generado noticias para mostrar.'
            },
            'API_NW_200': {
                description: 'Listado de las noticias generadas.',
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/createNews'
                        }
                    }
                }
            },
        }
    }
}
