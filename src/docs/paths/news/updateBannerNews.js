module.exports = {
    put: {
        tags: ['news'],
        description: "",
        operationId: "updateBannerNews",
        parameters: [
            {
                in: 'query',
                name: '_id  '
            }
        ],
        requestBody: {
            description: "Valor para actualizar el banner de una noticia.",
            content: {
                'applicaction/json': {
                    schema: {
                        $ref: '#/components/schemas/bannerBodyNews'
                    }
                }
            }
        },
        responses: {
            'API_NW_401': {
                description: 'No se almacenó correctamente la imagen.'
            },
            'API_NW_401': {
                description: 'Fallo al subir la imagen'
            },
            'API_NW_200': {
                description: 'Imagen actualizada con éxito',
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/responseBannerUpdate'
                        }
                    }
                }
            },
        }
    }
}

