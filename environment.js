require('dotenv').config();
module.exports = {
    PORT: process.env.PORT,
    FLLUYAPP_API: process.env.FLLUYAPP_API
}